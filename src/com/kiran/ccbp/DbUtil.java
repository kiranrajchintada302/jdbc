package com.kiran.ccbp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public final class DbUtil {
	private DbUtil() {
	}
	/*this prevents the creation of the object  private it not allow to create an object 
	 if it is public we can able to create an object of DbUtil to prevent that we used an private */

	public static Connection getConnection() throws SQLException, ClassNotFoundException {
		ResourceBundle rb = ResourceBundle.getBundle("com.kiran.ccbp.jdbc");
		//Class.forName(rb.getString("driverClassName"));
		String url = rb.getString("url");
		String username = rb.getString("username");
		String password = rb.getString("password");
		Connection connection = DriverManager.getConnection(url, username, password);

		return connection;
	}
}
/* we can directly use the methods in the DbUtil class  with out creating the object and infact we 
 not able to create an object also*/