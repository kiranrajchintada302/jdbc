package com.kiran.ccbp;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class InsertRecordUsingPrepareStatement {

	public static void main(String[] args) {

		int product_id = KeyboardUtil.getInt("Enter product_id: ");
		String product_name = KeyboardUtil.getString("Enter product_name: ");
		String descripton = KeyboardUtil.getString("Enter descripton: ");
		int price = KeyboardUtil.getInt("Enter price: ");
		String category = KeyboardUtil.getString("Enter category: ");

		String sql = "INSERT INTO product VALUES(?,?,?,?,?)";
//		String sql = "INSERT INTO product VALUES(?,?,?,?)";
		try(Connection conn = DbUtil.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql);
		)
		{
			
         	stmt.setInt(1, product_id);
			stmt.setString(2, product_name);
			stmt.setString(3, descripton);
			stmt.setInt(4, price);
			stmt.setString(5, category);
			stmt.executeUpdate();
			
//			stmt.setString(1, product_name);
//			stmt.setString(2, descripton);
//			stmt.setInt(3, price);
//			stmt.setString(4, category);
//			stmt.executeUpdate();
			System.out.println("Record inserted!!");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
