package com.kiran.ccbp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertNewEmployee {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {

		int empAge = KeyboardUtil.getInt("Enter employee age");
		String empName = KeyboardUtil.getString("Enter employee empName");
		String sql = "INSERT INTO employee(name,age) values(?,?)";
		//try {
			Connection conn = DbUtil.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, empName);
			stmt.setInt(2, empAge);

			stmt.executeUpdate();

			ResultSet keys = stmt.getGeneratedKeys();
			if (keys.next()) {
				int empId = keys.getInt(1);
				System.out.println("insertedEmployee empid :"+ keys.getInt(1));
			}
			conn.close();
			stmt.close();
	//	}catch (Exception ex) {
//			ex.printStackTrace();}

	}

}
