package com.kiran.ccbp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class GetAllProducts {
	public static void main(String[] args) {
		String sql = "SELECT * FROM product";
		try (Connection conn = DbUtil.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql);
				ResultSet rs = stmt.executeQuery();)

		{
			while (rs.next()) {
				String product_name = rs.getString("product_name");
				String category = rs.getString("category");
				System.out.println(product_name+"----->"+category);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
