package com.kiran.ccbp;

import java.util.Scanner;

@SuppressWarnings("resource")
public final class KeyboardUtil {
	private KeyboardUtil() {
	}

	public  static String getString(String message) {
		System.out.println(message);
		Scanner sc = new Scanner(System.in);
		return sc.nextLine();
	}
	 public static int getInt(String message) {
		System.out.println(message);
		Scanner sc = new Scanner(System.in);
		return sc.nextInt();
	}
}
