package com.kiran.ccbp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class GetSingleRecord {

	public static void main(String[] args) {

		int inputId = KeyboardUtil.getInt("Enter id of a product");
		String sql = "Select * from product where product_id=?";
		try (Connection conn = DbUtil.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql);) {

			stmt.setInt(1, inputId);
			try (ResultSet rs = stmt.executeQuery();) {
				if (rs.next()) {
					String name = rs.getString("product_name");
					int price = rs.getInt("price");
					System.out.println(name + " :" + price);
				} else {
					System.out.println("No result found with the id: " + inputId);
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
