package com.kiran.ccbp;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class InsertMultipleRecords {

	public static void main(String[] args) {
		String sql = "INSERT INTO product VALUES(?,?,?,?,?)";

		try (Connection conn = DbUtil.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql);) {
			
			while(true) {
				int product_id = KeyboardUtil.getInt("Enter product_id: ");
				String product_name = KeyboardUtil.getString("Enter product_name: ");
				String descripton = KeyboardUtil.getString("Enter descripton: ");
				int price = KeyboardUtil.getInt("Enter price: ");
				String category = KeyboardUtil.getString("Enter category: ");
				
				stmt.setInt(1, product_id);
				stmt.setString(2, product_name);
				stmt.setString(3, descripton);
				stmt.setInt(4, price);
				stmt.setString(5, category);
				stmt.addBatch();
				String ans= KeyboardUtil.getString("Do you want to insert another record yes/no");
				if(ans.equalsIgnoreCase("no")) {
					break;
				}
			}
			stmt.executeBatch();
			System.out.println("Batch inserted successfully11");

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
